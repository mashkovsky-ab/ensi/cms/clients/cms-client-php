# Ensi\CmsClient\BannersApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createBanners**](BannersApi.md#createBanners) | **POST** /contents/banners | Создание объекта типа Banner
[**deleteBannerFiles**](BannersApi.md#deleteBannerFiles) | **POST** /contents/banners/{id}:delete-file | Удаление файла для объекта типа Banner
[**deleteBanners**](BannersApi.md#deleteBanners) | **DELETE** /contents/banners/{id} | Удаление объекта типа Banner
[**replaceBanners**](BannersApi.md#replaceBanners) | **PUT** /contents/banners/{id} | Замена объекта типа Banner
[**searchBannerTypes**](BannersApi.md#searchBannerTypes) | **POST** /contents/banner-types:search | Поиск объектов типа BannerType
[**searchBanners**](BannersApi.md#searchBanners) | **POST** /contents/banners:search | Поиск объектов типа Banner
[**searchOneBanners**](BannersApi.md#searchOneBanners) | **POST** /contents/banners:search-one | Поиск объекта типа Banner
[**uploadBannerFiles**](BannersApi.md#uploadBannerFiles) | **POST** /contents/banners/{id}:upload-file | Загрузка файла для объекта типа Banner



## createBanners

> \Ensi\CmsClient\Dto\CreateBannersResponse createBanners($banner_for_create)

Создание объекта типа Banner

Создание объекта типа Banner

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\BannersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$banner_for_create = new \Ensi\CmsClient\Dto\BannerForCreate(); // \Ensi\CmsClient\Dto\BannerForCreate | 

try {
    $result = $apiInstance->createBanners($banner_for_create);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BannersApi->createBanners: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **banner_for_create** | [**\Ensi\CmsClient\Dto\BannerForCreate**](../Model/BannerForCreate.md)|  |

### Return type

[**\Ensi\CmsClient\Dto\CreateBannersResponse**](../Model/CreateBannersResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteBannerFiles

> \Ensi\CmsClient\Dto\EmptyDataResponse deleteBannerFiles($id, $delete_banner_file_request)

Удаление файла для объекта типа Banner

Удаление файла для объекта типа Banner

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\BannersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$delete_banner_file_request = new \Ensi\CmsClient\Dto\DeleteBannerFileRequest(); // \Ensi\CmsClient\Dto\DeleteBannerFileRequest | 

try {
    $result = $apiInstance->deleteBannerFiles($id, $delete_banner_file_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BannersApi->deleteBannerFiles: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **delete_banner_file_request** | [**\Ensi\CmsClient\Dto\DeleteBannerFileRequest**](../Model/DeleteBannerFileRequest.md)|  |

### Return type

[**\Ensi\CmsClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteBanners

> \Ensi\CmsClient\Dto\EmptyDataResponse deleteBanners($id)

Удаление объекта типа Banner

Удаление объекта типа Banner

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\BannersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deleteBanners($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BannersApi->deleteBanners: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\CmsClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## replaceBanners

> \Ensi\CmsClient\Dto\ReplaceBannersResponse replaceBanners($id, $banner_for_replace)

Замена объекта типа Banner

Замена объекта типа Banner

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\BannersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$banner_for_replace = new \Ensi\CmsClient\Dto\BannerForReplace(); // \Ensi\CmsClient\Dto\BannerForReplace | 

try {
    $result = $apiInstance->replaceBanners($id, $banner_for_replace);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BannersApi->replaceBanners: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **banner_for_replace** | [**\Ensi\CmsClient\Dto\BannerForReplace**](../Model/BannerForReplace.md)|  |

### Return type

[**\Ensi\CmsClient\Dto\ReplaceBannersResponse**](../Model/ReplaceBannersResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchBannerTypes

> \Ensi\CmsClient\Dto\SearchBannerTypesResponse searchBannerTypes($search_banner_types_request)

Поиск объектов типа BannerType

Поиск объектов типа BannerType

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\BannersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_banner_types_request = new \Ensi\CmsClient\Dto\SearchBannerTypesRequest(); // \Ensi\CmsClient\Dto\SearchBannerTypesRequest | 

try {
    $result = $apiInstance->searchBannerTypes($search_banner_types_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BannersApi->searchBannerTypes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_banner_types_request** | [**\Ensi\CmsClient\Dto\SearchBannerTypesRequest**](../Model/SearchBannerTypesRequest.md)|  |

### Return type

[**\Ensi\CmsClient\Dto\SearchBannerTypesResponse**](../Model/SearchBannerTypesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchBanners

> \Ensi\CmsClient\Dto\SearchBannersResponse searchBanners($search_banners_request)

Поиск объектов типа Banner

Поиск объектов типа Banner

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\BannersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_banners_request = new \Ensi\CmsClient\Dto\SearchBannersRequest(); // \Ensi\CmsClient\Dto\SearchBannersRequest | 

try {
    $result = $apiInstance->searchBanners($search_banners_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BannersApi->searchBanners: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_banners_request** | [**\Ensi\CmsClient\Dto\SearchBannersRequest**](../Model/SearchBannersRequest.md)|  |

### Return type

[**\Ensi\CmsClient\Dto\SearchBannersResponse**](../Model/SearchBannersResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchOneBanners

> \Ensi\CmsClient\Dto\SearchOneBannersResponse searchOneBanners($search_one_banners_request)

Поиск объекта типа Banner

Поиск объекта типа Banner

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\BannersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_one_banners_request = new \Ensi\CmsClient\Dto\SearchOneBannersRequest(); // \Ensi\CmsClient\Dto\SearchOneBannersRequest | 

try {
    $result = $apiInstance->searchOneBanners($search_one_banners_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BannersApi->searchOneBanners: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_one_banners_request** | [**\Ensi\CmsClient\Dto\SearchOneBannersRequest**](../Model/SearchOneBannersRequest.md)|  |

### Return type

[**\Ensi\CmsClient\Dto\SearchOneBannersResponse**](../Model/SearchOneBannersResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## uploadBannerFiles

> \Ensi\CmsClient\Dto\UploadBannerFilesResponse uploadBannerFiles($id, $type, $file)

Загрузка файла для объекта типа Banner

Загрузка файла для объекта типа Banner

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\BannersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$type = 'type_example'; // string | Тип изображения (значение из BannerImageTypeEnum)
$file = "/path/to/file.txt"; // \SplFileObject | Загружаемый файл

try {
    $result = $apiInstance->uploadBannerFiles($id, $type, $file);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BannersApi->uploadBannerFiles: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **type** | **string**| Тип изображения (значение из BannerImageTypeEnum) | [optional]
 **file** | **\SplFileObject****\SplFileObject**| Загружаемый файл | [optional]

### Return type

[**\Ensi\CmsClient\Dto\UploadBannerFilesResponse**](../Model/UploadBannerFilesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

