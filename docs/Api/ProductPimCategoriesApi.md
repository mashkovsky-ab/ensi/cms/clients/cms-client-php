# Ensi\CmsClient\ProductPimCategoriesApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createProductPimCategory**](ProductPimCategoriesApi.md#createProductPimCategory) | **POST** /contents/product-categories/pim-categories | Создание объекта типа ProductPimCategory
[**deleteProductPimCategory**](ProductPimCategoriesApi.md#deleteProductPimCategory) | **DELETE** /contents/product-categories/pim-categories/{id} | Запрос на удаление ProductPimCategory
[**getProductPimCategory**](ProductPimCategoriesApi.md#getProductPimCategory) | **GET** /contents/product-categories/pim-categories/{id} | Запрос ProductPimCategory по ID
[**replaceProductPimCategory**](ProductPimCategoriesApi.md#replaceProductPimCategory) | **PUT** /contents/product-categories/pim-categories/{id} | Замена объекта типа ProductPimCategory
[**searchProductPimCategory**](ProductPimCategoriesApi.md#searchProductPimCategory) | **POST** /contents/product-categories/pim-categories:search | Поиск объектов типа ProductPimCategory



## createProductPimCategory

> \Ensi\CmsClient\Dto\CreateProductPimCategoriesResponse createProductPimCategory($product_pim_category_for_create)

Создание объекта типа ProductPimCategory

Создание объекта типа ProductPimCategory

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\ProductPimCategoriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$product_pim_category_for_create = new \Ensi\CmsClient\Dto\ProductPimCategoryForCreate(); // \Ensi\CmsClient\Dto\ProductPimCategoryForCreate | 

try {
    $result = $apiInstance->createProductPimCategory($product_pim_category_for_create);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductPimCategoriesApi->createProductPimCategory: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **product_pim_category_for_create** | [**\Ensi\CmsClient\Dto\ProductPimCategoryForCreate**](../Model/ProductPimCategoryForCreate.md)|  |

### Return type

[**\Ensi\CmsClient\Dto\CreateProductPimCategoriesResponse**](../Model/CreateProductPimCategoriesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteProductPimCategory

> \Ensi\CmsClient\Dto\EmptyDataResponse deleteProductPimCategory($id)

Запрос на удаление ProductPimCategory

Запрос на удаление ProductPimCategory

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\ProductPimCategoriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deleteProductPimCategory($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductPimCategoriesApi->deleteProductPimCategory: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\CmsClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getProductPimCategory

> \Ensi\CmsClient\Dto\ProductPimCategoryResponse getProductPimCategory($id, $include)

Запрос ProductPimCategory по ID

Запрос ProductPimCategory по ID

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\ProductPimCategoriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$include = 'include_example'; // string | Связанные сущности для подгрузки, через запятую

try {
    $result = $apiInstance->getProductPimCategory($id, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductPimCategoriesApi->getProductPimCategory: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **include** | **string**| Связанные сущности для подгрузки, через запятую | [optional]

### Return type

[**\Ensi\CmsClient\Dto\ProductPimCategoryResponse**](../Model/ProductPimCategoryResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## replaceProductPimCategory

> \Ensi\CmsClient\Dto\ReplaceProductPimCategoriesResponse replaceProductPimCategory($id, $product_pim_category_for_replace)

Замена объекта типа ProductPimCategory

Замена объекта типа ProductPimCategory

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\ProductPimCategoriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$product_pim_category_for_replace = new \Ensi\CmsClient\Dto\ProductPimCategoryForReplace(); // \Ensi\CmsClient\Dto\ProductPimCategoryForReplace | 

try {
    $result = $apiInstance->replaceProductPimCategory($id, $product_pim_category_for_replace);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductPimCategoriesApi->replaceProductPimCategory: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **product_pim_category_for_replace** | [**\Ensi\CmsClient\Dto\ProductPimCategoryForReplace**](../Model/ProductPimCategoryForReplace.md)|  |

### Return type

[**\Ensi\CmsClient\Dto\ReplaceProductPimCategoriesResponse**](../Model/ReplaceProductPimCategoriesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchProductPimCategory

> \Ensi\CmsClient\Dto\SearchProductPimCategoriesResponse searchProductPimCategory($search_product_pim_categories_request)

Поиск объектов типа ProductPimCategory

Поиск объектов типа ProductPimCategory

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\ProductPimCategoriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_product_pim_categories_request = new \Ensi\CmsClient\Dto\SearchProductPimCategoriesRequest(); // \Ensi\CmsClient\Dto\SearchProductPimCategoriesRequest | 

try {
    $result = $apiInstance->searchProductPimCategory($search_product_pim_categories_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductPimCategoriesApi->searchProductPimCategory: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_product_pim_categories_request** | [**\Ensi\CmsClient\Dto\SearchProductPimCategoriesRequest**](../Model/SearchProductPimCategoriesRequest.md)|  |

### Return type

[**\Ensi\CmsClient\Dto\SearchProductPimCategoriesResponse**](../Model/SearchProductPimCategoriesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

