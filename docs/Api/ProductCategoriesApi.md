# Ensi\CmsClient\ProductCategoriesApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createProductCategory**](ProductCategoriesApi.md#createProductCategory) | **POST** /contents/product-categories | Создание объекта типа ProductCategory
[**deleteProductCategory**](ProductCategoriesApi.md#deleteProductCategory) | **DELETE** /contents/product-categories/{id} | Запрос на удаление ProductCategory
[**getProductCategoriesTree**](ProductCategoriesApi.md#getProductCategoriesTree) | **POST** /contents/product-categories:tree | Формирование дерева категорий
[**getProductCategory**](ProductCategoriesApi.md#getProductCategory) | **GET** /contents/product-categories/{id} | Запрос ProductCategory по ID
[**massDeleteProductCategory**](ProductCategoriesApi.md#massDeleteProductCategory) | **POST** /contents/product-categories:mass-delete | Массовое удаление объектов типа ProductCategory
[**replaceProductCategory**](ProductCategoriesApi.md#replaceProductCategory) | **PUT** /contents/product-categories/{id} | Замена объекта типа ProductCategory
[**searchProductCategories**](ProductCategoriesApi.md#searchProductCategories) | **POST** /contents/product-categories:search | Поиск объектов типа ProductCategory



## createProductCategory

> \Ensi\CmsClient\Dto\CreateProductCategoriesResponse createProductCategory($product_category_for_create)

Создание объекта типа ProductCategory

Создание объекта типа ProductCategory

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\ProductCategoriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$product_category_for_create = new \Ensi\CmsClient\Dto\ProductCategoryForCreate(); // \Ensi\CmsClient\Dto\ProductCategoryForCreate | 

try {
    $result = $apiInstance->createProductCategory($product_category_for_create);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductCategoriesApi->createProductCategory: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **product_category_for_create** | [**\Ensi\CmsClient\Dto\ProductCategoryForCreate**](../Model/ProductCategoryForCreate.md)|  |

### Return type

[**\Ensi\CmsClient\Dto\CreateProductCategoriesResponse**](../Model/CreateProductCategoriesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteProductCategory

> \Ensi\CmsClient\Dto\EmptyDataResponse deleteProductCategory($id)

Запрос на удаление ProductCategory

Запрос на удаление ProductCategory

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\ProductCategoriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deleteProductCategory($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductCategoriesApi->deleteProductCategory: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\CmsClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getProductCategoriesTree

> \Ensi\CmsClient\Dto\ProductCategoriesTreeResponse getProductCategoriesTree($product_categories_tree_request)

Формирование дерева категорий

Формирование дерева категорий

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\ProductCategoriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$product_categories_tree_request = new \Ensi\CmsClient\Dto\ProductCategoriesTreeRequest(); // \Ensi\CmsClient\Dto\ProductCategoriesTreeRequest | 

try {
    $result = $apiInstance->getProductCategoriesTree($product_categories_tree_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductCategoriesApi->getProductCategoriesTree: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **product_categories_tree_request** | [**\Ensi\CmsClient\Dto\ProductCategoriesTreeRequest**](../Model/ProductCategoriesTreeRequest.md)|  |

### Return type

[**\Ensi\CmsClient\Dto\ProductCategoriesTreeResponse**](../Model/ProductCategoriesTreeResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getProductCategory

> \Ensi\CmsClient\Dto\ProductCategoryResponse getProductCategory($id, $include)

Запрос ProductCategory по ID

Запрос ProductCategory по ID

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\ProductCategoriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$include = 'include_example'; // string | Связанные сущности для подгрузки, через запятую

try {
    $result = $apiInstance->getProductCategory($id, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductCategoriesApi->getProductCategory: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **include** | **string**| Связанные сущности для подгрузки, через запятую | [optional]

### Return type

[**\Ensi\CmsClient\Dto\ProductCategoryResponse**](../Model/ProductCategoryResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## massDeleteProductCategory

> \Ensi\CmsClient\Dto\EmptyDataResponse massDeleteProductCategory($mass_delete_product_categories_request)

Массовое удаление объектов типа ProductCategory

Массовое удаление объектов типа ProductCategory

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\ProductCategoriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$mass_delete_product_categories_request = new \Ensi\CmsClient\Dto\MassDeleteProductCategoriesRequest(); // \Ensi\CmsClient\Dto\MassDeleteProductCategoriesRequest | 

try {
    $result = $apiInstance->massDeleteProductCategory($mass_delete_product_categories_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductCategoriesApi->massDeleteProductCategory: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mass_delete_product_categories_request** | [**\Ensi\CmsClient\Dto\MassDeleteProductCategoriesRequest**](../Model/MassDeleteProductCategoriesRequest.md)|  |

### Return type

[**\Ensi\CmsClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## replaceProductCategory

> \Ensi\CmsClient\Dto\CreateProductCategoriesResponse replaceProductCategory($id, $product_category_for_replace)

Замена объекта типа ProductCategory

Замена объекта типа ProductCategory

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\ProductCategoriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$product_category_for_replace = new \Ensi\CmsClient\Dto\ProductCategoryForReplace(); // \Ensi\CmsClient\Dto\ProductCategoryForReplace | 

try {
    $result = $apiInstance->replaceProductCategory($id, $product_category_for_replace);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductCategoriesApi->replaceProductCategory: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **product_category_for_replace** | [**\Ensi\CmsClient\Dto\ProductCategoryForReplace**](../Model/ProductCategoryForReplace.md)|  |

### Return type

[**\Ensi\CmsClient\Dto\CreateProductCategoriesResponse**](../Model/CreateProductCategoriesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchProductCategories

> \Ensi\CmsClient\Dto\SearchProductCategoriesResponse searchProductCategories($search_product_categories_request)

Поиск объектов типа ProductCategory

Поиск объектов типа ProductCategory

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\ProductCategoriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_product_categories_request = new \Ensi\CmsClient\Dto\SearchProductCategoriesRequest(); // \Ensi\CmsClient\Dto\SearchProductCategoriesRequest | 

try {
    $result = $apiInstance->searchProductCategories($search_product_categories_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductCategoriesApi->searchProductCategories: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_product_categories_request** | [**\Ensi\CmsClient\Dto\SearchProductCategoriesRequest**](../Model/SearchProductCategoriesRequest.md)|  |

### Return type

[**\Ensi\CmsClient\Dto\SearchProductCategoriesResponse**](../Model/SearchProductCategoriesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

