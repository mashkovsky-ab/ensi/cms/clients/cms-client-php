# Ensi\CmsClient\MenusApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**searchMenus**](MenusApi.md#searchMenus) | **POST** /contents/menus:search | Поиск объектов типа Menu
[**searchOneMenus**](MenusApi.md#searchOneMenus) | **POST** /contents/menus:search-one | Поиск объекта типа Menu
[**updateMenuTrees**](MenusApi.md#updateMenuTrees) | **PUT** /contents/menus/{id}/trees | Обновление дерева Menu



## searchMenus

> \Ensi\CmsClient\Dto\SearchMenusResponse searchMenus($search_menus_request)

Поиск объектов типа Menu

Поиск объектов типа Menu

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\MenusApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_menus_request = new \Ensi\CmsClient\Dto\SearchMenusRequest(); // \Ensi\CmsClient\Dto\SearchMenusRequest | 

try {
    $result = $apiInstance->searchMenus($search_menus_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MenusApi->searchMenus: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_menus_request** | [**\Ensi\CmsClient\Dto\SearchMenusRequest**](../Model/SearchMenusRequest.md)|  |

### Return type

[**\Ensi\CmsClient\Dto\SearchMenusResponse**](../Model/SearchMenusResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchOneMenus

> \Ensi\CmsClient\Dto\SearchOneMenusResponse searchOneMenus($search_one_menus_request)

Поиск объекта типа Menu

Поиск объекта типа Menu

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\MenusApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_one_menus_request = new \Ensi\CmsClient\Dto\SearchOneMenusRequest(); // \Ensi\CmsClient\Dto\SearchOneMenusRequest | 

try {
    $result = $apiInstance->searchOneMenus($search_one_menus_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MenusApi->searchOneMenus: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_one_menus_request** | [**\Ensi\CmsClient\Dto\SearchOneMenusRequest**](../Model/SearchOneMenusRequest.md)|  |

### Return type

[**\Ensi\CmsClient\Dto\SearchOneMenusResponse**](../Model/SearchOneMenusResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## updateMenuTrees

> \Ensi\CmsClient\Dto\UpdateMenuTreesResponse updateMenuTrees($id, $update_menu_trees_request)

Обновление дерева Menu

Обновление дерева Menu

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\MenusApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$update_menu_trees_request = new \Ensi\CmsClient\Dto\UpdateMenuTreesRequest(); // \Ensi\CmsClient\Dto\UpdateMenuTreesRequest | 

try {
    $result = $apiInstance->updateMenuTrees($id, $update_menu_trees_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MenusApi->updateMenuTrees: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **update_menu_trees_request** | [**\Ensi\CmsClient\Dto\UpdateMenuTreesRequest**](../Model/UpdateMenuTreesRequest.md)|  |

### Return type

[**\Ensi\CmsClient\Dto\UpdateMenuTreesResponse**](../Model/UpdateMenuTreesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

