# Ensi\CmsClient\ProductGroupsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createProductGroups**](ProductGroupsApi.md#createProductGroups) | **POST** /contents/product-groups | Создание объекта типа ProductGroup
[**deleteProductGroupFiles**](ProductGroupsApi.md#deleteProductGroupFiles) | **POST** /contents/product-groups/{id}:delete-file | Удаление файла для объекта типа ProductGroup
[**deleteProductGroups**](ProductGroupsApi.md#deleteProductGroups) | **DELETE** /contents/product-groups/{id} | Удаление объекта типа ProductGroup
[**replaceProductGroups**](ProductGroupsApi.md#replaceProductGroups) | **PUT** /contents/product-groups/{id} | Замена объекта типа ProductGroup
[**searchOneProductGroups**](ProductGroupsApi.md#searchOneProductGroups) | **POST** /contents/product-groups:search-one | Поиск объекта типа ProductGroup
[**searchProductGroupTypes**](ProductGroupsApi.md#searchProductGroupTypes) | **POST** /contents/product-group-types:search | Поиск объектов типа ProductGroupType
[**searchProductGroups**](ProductGroupsApi.md#searchProductGroups) | **POST** /contents/product-groups:search | Поиск объектов типа ProductGroup
[**uploadProductGroupFiles**](ProductGroupsApi.md#uploadProductGroupFiles) | **POST** /contents/product-groups/{id}:upload-file | Загрузка файла для объекта типа ProductGroup



## createProductGroups

> \Ensi\CmsClient\Dto\CreateProductGroupsResponse createProductGroups($product_group_for_create)

Создание объекта типа ProductGroup

Создание объекта типа ProductGroup

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\ProductGroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$product_group_for_create = new \Ensi\CmsClient\Dto\ProductGroupForCreate(); // \Ensi\CmsClient\Dto\ProductGroupForCreate | 

try {
    $result = $apiInstance->createProductGroups($product_group_for_create);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductGroupsApi->createProductGroups: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **product_group_for_create** | [**\Ensi\CmsClient\Dto\ProductGroupForCreate**](../Model/ProductGroupForCreate.md)|  |

### Return type

[**\Ensi\CmsClient\Dto\CreateProductGroupsResponse**](../Model/CreateProductGroupsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteProductGroupFiles

> \Ensi\CmsClient\Dto\EmptyDataResponse deleteProductGroupFiles($id)

Удаление файла для объекта типа ProductGroup

Удаление файла для объекта типа ProductGroup

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\ProductGroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deleteProductGroupFiles($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductGroupsApi->deleteProductGroupFiles: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\CmsClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteProductGroups

> \Ensi\CmsClient\Dto\EmptyDataResponse deleteProductGroups($id)

Удаление объекта типа ProductGroup

Удаление объекта типа ProductGroup

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\ProductGroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deleteProductGroups($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductGroupsApi->deleteProductGroups: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\CmsClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## replaceProductGroups

> \Ensi\CmsClient\Dto\ReplaceProductGroupsResponse replaceProductGroups($id, $product_group_for_replace)

Замена объекта типа ProductGroup

Замена объекта типа ProductGroup

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\ProductGroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$product_group_for_replace = new \Ensi\CmsClient\Dto\ProductGroupForReplace(); // \Ensi\CmsClient\Dto\ProductGroupForReplace | 

try {
    $result = $apiInstance->replaceProductGroups($id, $product_group_for_replace);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductGroupsApi->replaceProductGroups: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **product_group_for_replace** | [**\Ensi\CmsClient\Dto\ProductGroupForReplace**](../Model/ProductGroupForReplace.md)|  |

### Return type

[**\Ensi\CmsClient\Dto\ReplaceProductGroupsResponse**](../Model/ReplaceProductGroupsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchOneProductGroups

> \Ensi\CmsClient\Dto\SearchOneProductGroupsResponse searchOneProductGroups($search_one_product_groups_request)

Поиск объекта типа ProductGroup

Поиск объекта типа ProductGroup

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\ProductGroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_one_product_groups_request = new \Ensi\CmsClient\Dto\SearchOneProductGroupsRequest(); // \Ensi\CmsClient\Dto\SearchOneProductGroupsRequest | 

try {
    $result = $apiInstance->searchOneProductGroups($search_one_product_groups_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductGroupsApi->searchOneProductGroups: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_one_product_groups_request** | [**\Ensi\CmsClient\Dto\SearchOneProductGroupsRequest**](../Model/SearchOneProductGroupsRequest.md)|  |

### Return type

[**\Ensi\CmsClient\Dto\SearchOneProductGroupsResponse**](../Model/SearchOneProductGroupsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchProductGroupTypes

> \Ensi\CmsClient\Dto\SearchProductGroupTypesResponse searchProductGroupTypes($search_product_group_types_request)

Поиск объектов типа ProductGroupType

Поиск объектов типа ProductGroupType

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\ProductGroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_product_group_types_request = new \Ensi\CmsClient\Dto\SearchProductGroupTypesRequest(); // \Ensi\CmsClient\Dto\SearchProductGroupTypesRequest | 

try {
    $result = $apiInstance->searchProductGroupTypes($search_product_group_types_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductGroupsApi->searchProductGroupTypes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_product_group_types_request** | [**\Ensi\CmsClient\Dto\SearchProductGroupTypesRequest**](../Model/SearchProductGroupTypesRequest.md)|  |

### Return type

[**\Ensi\CmsClient\Dto\SearchProductGroupTypesResponse**](../Model/SearchProductGroupTypesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchProductGroups

> \Ensi\CmsClient\Dto\SearchProductGroupsResponse searchProductGroups($search_product_groups_request)

Поиск объектов типа ProductGroup

Поиск объектов типа ProductGroup

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\ProductGroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_product_groups_request = new \Ensi\CmsClient\Dto\SearchProductGroupsRequest(); // \Ensi\CmsClient\Dto\SearchProductGroupsRequest | 

try {
    $result = $apiInstance->searchProductGroups($search_product_groups_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductGroupsApi->searchProductGroups: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_product_groups_request** | [**\Ensi\CmsClient\Dto\SearchProductGroupsRequest**](../Model/SearchProductGroupsRequest.md)|  |

### Return type

[**\Ensi\CmsClient\Dto\SearchProductGroupsResponse**](../Model/SearchProductGroupsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## uploadProductGroupFiles

> \Ensi\CmsClient\Dto\UploadProductGroupFilesResponse uploadProductGroupFiles($id, $file)

Загрузка файла для объекта типа ProductGroup

Загрузка файла для объекта типа ProductGroup

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\ProductGroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$file = "/path/to/file.txt"; // \SplFileObject | Загружаемый файл

try {
    $result = $apiInstance->uploadProductGroupFiles($id, $file);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductGroupsApi->uploadProductGroupFiles: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **file** | **\SplFileObject****\SplFileObject**| Загружаемый файл | [optional]

### Return type

[**\Ensi\CmsClient\Dto\UploadProductGroupFilesResponse**](../Model/UploadProductGroupFilesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

