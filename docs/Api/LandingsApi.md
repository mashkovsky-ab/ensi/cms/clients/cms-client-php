# Ensi\CmsClient\LandingsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createLandings**](LandingsApi.md#createLandings) | **POST** /contents/landings | Создание объекта типа Landing
[**deleteLandings**](LandingsApi.md#deleteLandings) | **DELETE** /contents/landings/{id} | Удаление объекта типа Landing
[**getLandingWidgets**](LandingsApi.md#getLandingWidgets) | **GET** /contents/landing-widgets | Получить список виджетов
[**replaceLandings**](LandingsApi.md#replaceLandings) | **PUT** /contents/landings/{id} | Замена объекта типа Landing
[**searchLandings**](LandingsApi.md#searchLandings) | **POST** /contents/landings:search | Поиск объектов типа Landing



## createLandings

> \Ensi\CmsClient\Dto\CreateLandingsResponse createLandings($landing_for_create)

Создание объекта типа Landing

Создание объекта типа Landing

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\LandingsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$landing_for_create = new \Ensi\CmsClient\Dto\LandingForCreate(); // \Ensi\CmsClient\Dto\LandingForCreate | 

try {
    $result = $apiInstance->createLandings($landing_for_create);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling LandingsApi->createLandings: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **landing_for_create** | [**\Ensi\CmsClient\Dto\LandingForCreate**](../Model/LandingForCreate.md)|  |

### Return type

[**\Ensi\CmsClient\Dto\CreateLandingsResponse**](../Model/CreateLandingsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteLandings

> \Ensi\CmsClient\Dto\EmptyDataResponse deleteLandings($id)

Удаление объекта типа Landing

Удаление объекта типа Landing

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\LandingsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deleteLandings($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling LandingsApi->deleteLandings: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\CmsClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getLandingWidgets

> \Ensi\CmsClient\Dto\ListLandingsResponse getLandingWidgets()

Получить список виджетов

Получить список виджетов

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\LandingsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getLandingWidgets();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling LandingsApi->getLandingWidgets: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\Ensi\CmsClient\Dto\ListLandingsResponse**](../Model/ListLandingsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## replaceLandings

> \Ensi\CmsClient\Dto\ReplaceLandingsResponse replaceLandings($id, $landing_for_replace)

Замена объекта типа Landing

Замена объекта типа Landing

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\LandingsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$landing_for_replace = new \Ensi\CmsClient\Dto\LandingForReplace(); // \Ensi\CmsClient\Dto\LandingForReplace | 

try {
    $result = $apiInstance->replaceLandings($id, $landing_for_replace);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling LandingsApi->replaceLandings: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **landing_for_replace** | [**\Ensi\CmsClient\Dto\LandingForReplace**](../Model/LandingForReplace.md)|  |

### Return type

[**\Ensi\CmsClient\Dto\ReplaceLandingsResponse**](../Model/ReplaceLandingsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchLandings

> \Ensi\CmsClient\Dto\SearchLandingsResponse searchLandings($search_landings_request)

Поиск объектов типа Landing

Поиск объектов типа Landing

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CmsClient\Api\LandingsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_landings_request = new \Ensi\CmsClient\Dto\SearchLandingsRequest(); // \Ensi\CmsClient\Dto\SearchLandingsRequest | 

try {
    $result = $apiInstance->searchLandings($search_landings_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling LandingsApi->searchLandings: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_landings_request** | [**\Ensi\CmsClient\Dto\SearchLandingsRequest**](../Model/SearchLandingsRequest.md)|  |

### Return type

[**\Ensi\CmsClient\Dto\SearchLandingsResponse**](../Model/SearchLandingsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

