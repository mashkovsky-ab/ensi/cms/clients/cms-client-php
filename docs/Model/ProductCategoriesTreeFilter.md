# # ProductCategoriesTreeFilter

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**root_id** | **int** | Идентификатор категории с которой начинать загрузку | [optional] 
**active** | **bool** | Выбирать только активные категории | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


