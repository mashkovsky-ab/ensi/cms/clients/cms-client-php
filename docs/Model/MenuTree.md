# # MenuTree

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** | Название пункта | [optional] 
**url** | **string** | Ссылка | [optional] 
**active** | **bool** | Видимость пункта меню | [optional] 
**children** | **object[]** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


