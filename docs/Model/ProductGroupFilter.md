# # ProductGroupFilter

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор | [optional] 
**product_group_id** | **int** | Идентификатор продуктовой группы | [optional] 
**code** | **string** | Код | [optional] 
**value** | **string** | Значение | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


