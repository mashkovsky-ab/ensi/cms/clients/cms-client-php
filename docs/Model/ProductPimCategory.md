# # ProductPimCategory

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор | [optional] 
**product_category_id** | **int** | Идентификатор продуктовой категории в CMS | [optional] 
**code** | **string** | Код категории в ПИМе | [optional] 
**filters** | [**\Ensi\CmsClient\Dto\ProductPimCategoryFilter[]**](ProductPimCategoryFilter.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


