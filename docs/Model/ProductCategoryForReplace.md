# # ProductCategoryForReplace

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** | Наименование | [optional] 
**url** | **string** | Url категории | [optional] 
**active** | **bool** | Активность | [optional] 
**order** | **int** | Порядок категорий одного уровня. По умолчанию 0 | [optional] 
**parent_id** | **int** | Идентификатор родительской категории | [optional] 
**pim_categories** | [**\Ensi\CmsClient\Dto\ProductPimCategoryForCreate[]**](ProductPimCategoryForCreate.md) | У категорий ПИМа product_category_id выставится автоматически | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


