# # ProductGroupProduct

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор | [optional] 
**product_group_id** | **int** | Идентификатор продуктовой группы | [optional] 
**sort** | **int** | Значение | [optional] 
**product_id** | **int** | Идентификатор товара | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


