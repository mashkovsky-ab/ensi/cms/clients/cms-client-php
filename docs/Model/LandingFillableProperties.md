# # LandingFillableProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** | Название | [optional] 
**active** | **bool** | Активность | [optional] 
**code** | **string** | Код | [optional] 
**widgets** | **object[]** | Виджеты | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


