# # LandingWidget

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** | Имя | [optional] 
**widget_code** | **string** | Код виджета | [optional] 
**component** | **string** | Код компонента | [optional] 
**preview_big** | **object** | Превью изображение | [optional] 
**preview_small** | **object** | Мини изображение | [optional] 
**props** | [**object**](.md) | Свойства | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


