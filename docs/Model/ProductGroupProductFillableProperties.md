# # ProductGroupProductFillableProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sort** | **int** | Значение | [optional] 
**product_id** | **int** | Идентификатор товара | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


