# # ProductCategoriesTreeItem

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор категории | [optional] 
**name** | **string** | Название категории | [optional] 
**url** | **string** | Url категории | [optional] 
**active** | **bool** | Активность | [optional] 
**order** | **int** | Порядок категорий одного уровня. По умолчанию 0 | [optional] 
**pim_categories** | [**\Ensi\CmsClient\Dto\ProductPimCategory[]**](ProductPimCategory.md) |  | [optional] 
**children** | **object[]** | Дочерние категории, если есть | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


