# # MenuIncludes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**\Ensi\CmsClient\Dto\MenuItem[]**](MenuItem.md) | Пункты меню | [optional] 
**items_tree** | [**\Ensi\CmsClient\Dto\MenuTree[]**](MenuTree.md) | Представление пунктов меню (items) в виде дерева | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


