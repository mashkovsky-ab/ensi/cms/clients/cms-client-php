# # BannerType

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор | [optional] 
**code** | **string** | Код (значение из BannerTypeEnum) | [optional] 
**active** | **bool** | Активность | [optional] 
**name** | **string** | Значение | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


