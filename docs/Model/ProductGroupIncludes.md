# # ProductGroupIncludes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | [**\Ensi\CmsClient\Dto\ProductGroupType**](ProductGroupType.md) |  | [optional] 
**banner** | [**\Ensi\CmsClient\Dto\Banner**](Banner.md) |  | [optional] 
**filters** | [**\Ensi\CmsClient\Dto\ProductGroupFilter[]**](ProductGroupFilter.md) |  | [optional] 
**products** | [**\Ensi\CmsClient\Dto\ProductGroupProduct[]**](ProductGroupProduct.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


