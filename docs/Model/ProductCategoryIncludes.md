# # ProductCategoryIncludes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pim_categories** | [**\Ensi\CmsClient\Dto\ProductPimCategory[]**](ProductPimCategory.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


