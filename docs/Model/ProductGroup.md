# # ProductGroup

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор | [optional] 
**preview_photo** | [**\Ensi\CmsClient\Dto\File**](File.md) |  | [optional] 
**name** | **string** | Наименование | [optional] 
**code** | **string** | Символьный код | [optional] 
**active** | **bool** | Активность | [optional] 
**is_shown** | **bool** | Доступность для показа | [optional] 
**type_id** | **int** | Идентификатор типа | [optional] 
**banner_id** | **int** | Идентификатор баннера | [optional] 
**category_code** | **string** | Код категории | [optional] 
**filters** | [**\Ensi\CmsClient\Dto\ProductGroupFilter[]**](ProductGroupFilter.md) |  | [optional] 
**products** | [**\Ensi\CmsClient\Dto\ProductGroupProduct[]**](ProductGroupProduct.md) |  | [optional] 
**type** | [**\Ensi\CmsClient\Dto\ProductGroupType**](ProductGroupType.md) |  | [optional] 
**banner** | [**\Ensi\CmsClient\Dto\Banner**](Banner.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


