# # SearchProductGroupTypesResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**\Ensi\CmsClient\Dto\ProductGroupType[]**](ProductGroupType.md) |  | 
**meta** | [**\Ensi\CmsClient\Dto\SearchProductGroupsResponseMeta**](SearchProductGroupsResponseMeta.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


