# # ProductGroupForReplace

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** | Наименование | [optional] 
**code** | **string** | Символьный код | [optional] 
**active** | **bool** | Активность | [optional] 
**is_shown** | **bool** | Доступность для показа | [optional] 
**type_id** | **int** | Идентификатор типа | [optional] 
**banner_id** | **int** | Идентификатор баннера | [optional] 
**category_code** | **string** | Код категории | [optional] 
**filters** | [**\Ensi\CmsClient\Dto\ProductGroupFilterForCreate[]**](ProductGroupFilterForCreate.md) |  | [optional] 
**products** | [**\Ensi\CmsClient\Dto\ProductGroupProductForCreate[]**](ProductGroupProductForCreate.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


