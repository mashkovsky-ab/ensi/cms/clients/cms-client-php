# # MenuItem

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор | [optional] 
**url** | **string** | Ссылка | [optional] 
**name** | **string** | Название | [optional] 
**menu_id** | **int** | Идентификатор меню | [optional] 
**_lft** | **int** | Идентификатор элемента слева | [optional] 
**_rgt** | **int** | Идентификатор элемента справа | [optional] 
**parent_id** | **int** | Идентификатор родительского элемента | [optional] 
**active** | **bool** | Видимость пункта меню | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


