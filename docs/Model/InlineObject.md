# # InlineObject

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **string** | Тип изображения (значение из BannerImageTypeEnum) | [optional] 
**file** | [**\SplFileObject**](\SplFileObject.md) | Загружаемый файл | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


