# # ProductGroupTypeFillableProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **string** | Код (значение из ProductGroupTypeEnum) | [optional] 
**name** | **string** | Значение | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


