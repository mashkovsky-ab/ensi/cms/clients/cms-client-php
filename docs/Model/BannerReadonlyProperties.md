# # BannerReadonlyProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор | [optional] 
**desktop_image** | [**\Ensi\CmsClient\Dto\File**](File.md) |  | [optional] 
**tablet_image** | [**\Ensi\CmsClient\Dto\File**](File.md) |  | [optional] 
**mobile_image** | [**\Ensi\CmsClient\Dto\File**](File.md) |  | [optional] 
**button_id** | **int** | Идентификатор кнопки | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


